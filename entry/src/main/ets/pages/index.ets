/**
 * MIT License
 *
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import lottie from '@ohos/lottie'
import { AnimationItem, AnimationEventCallback } from '@ohos/lottie'

@Entry
@Component
struct Index {
  @State animationDuration: string = '获取动画时长'
  @State pauseState: string = '判断当前动画是否正在运行'
  @State isCanvasVisible: Visibility = Visibility.Visible;
  private renderingSettings: RenderingContextSettings = new RenderingContextSettings(true)
  private canvasRenderingContext: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.renderingSettings)
  private mainRenderingSettings: RenderingContextSettings = new RenderingContextSettings(true)
  private mainCanvasRenderingContext: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.mainRenderingSettings)
  private animateItem: AnimationItem = null;
  private animateName: string = "grunt";
  private callbackItem: AnimationEventCallback = function () {
    console.info("grunt loopComplete");
  };

  aboutToAppear(): void {
    console.info('aboutToAppear');
  }

  /**
   * 页面销毁时释放动画资源
   */
  aboutToDisappear(): void {
    console.info('aboutToDisappear');
    lottie.destroy();
  }

  onPageShow(): void {
    console.info('onPageShow');
    lottie.play();
  }

  onPageHide(): void {
    console.info('onPageHide');
    lottie.pause();
  }

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Row({ space: 10 }) {
        Canvas(this.canvasRenderingContext)
          .width('50%')
          .height(360 + 'px')
          .backgroundColor(Color.Gray)
          .onDisAppear(() => {
            // 组件移除时，可销毁动画资源
            lottie.destroy("2016");
          })

        Canvas(this.mainCanvasRenderingContext)
          .width('50%')
          .height(360 + 'px')
          .backgroundColor(Color.Gray)
          .onReady(() => {
            // 可在此生命回调周期中加载动画，可以保证动画尺寸正确
          })
          .onDisAppear(() => {
            lottie.destroy(this.animateName);
          })
          .visibility(this.isCanvasVisible)
      }

      Row() {
        Button('加载2016')
          .onClick(() => {
            lottie.loadAnimation({
              container: this.canvasRenderingContext,
              renderer: 'canvas', // canvas 渲染模式
              loop: true,
              autoplay: true,
              name: '2016',
              path: "common/lottie/data.json", // 路径加载动画只支持entry/src/main/ets 文件夹下的相对路径
            })
          })
      }.margin({ top: 5 })

      Row() {
        Button('加载json')
          .onClick(() => {
            this.animateItem = lottie.loadAnimation({
              container: this.mainCanvasRenderingContext,
              renderer: 'svg', // svg 渲染模式
              loop: 10,
              autoplay: true,
              name: this.animateName,
              path: "common/lottie/grunt.json", // 路径加载动画只支持entry/src/main/ets 文件夹下的相对路径
            })
            this.animateItem.addEventListener('enterFrame', function () {
              console.info("lottie enterFrame");
            }); //只要播放，会一直触发
            this.animateItem.addEventListener('loopComplete', function () {
              console.info("lottie loopComplete");
            }); //动画播放一遍结束触发
            this.animateItem.addEventListener('complete', function () {
              console.info("lottie complete");
            }); //动画播放结束且不再播放动画触发
            this.animateItem.addEventListener('destroy', function () {
              console.info("lottie destroy");
            }); //删除动画触发
            this.animateItem.addEventListener('DOMLoaded', function () {
              console.info("lottie DOMLoaded");
            }); //动画加载完成，播放之前触发
          })

        Button('播放')
          .onClick(() => {
            lottie.play();
          })

        Button('暂停')
          .onClick(() => {
            lottie.pause();
          })
      }.margin({ top: 5 })

      Row() {
        Button('结束，并回到第0帧')
          .onClick(() => {
            lottie.stop();
          })

        Button('切换暂停')
          .onClick(() => {
            lottie.togglePause();
          })
      }.margin({ top: 5 })

      Row() {
        Button('跳到某一帧并停止')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.goToAndStop(250, true);
            }
          })

        Button('跳到某一秒并停止')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.goToAndStop(5000, false);
            }
          })
      }.margin({ top: 5 })

      Row() {
        Button('跳到某一帧并播放')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.goToAndPlay(250, true);
            }
          })

        Button('跳到某一秒并播放')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.goToAndPlay(12000, false);
            }
          })
      }.margin({ top: 5 })

      Row() {
        Button('设置片段:5-15帧')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.setSegment(5, 15);
            }
          })

        Button('设置片段:20-30帧')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.setSegment(20, 30);
            }
          })
      }.margin({ top: 5 })

      Row() {
        Button('播放5-15帧和20-30帧')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.playSegments([[5, 15], [20, 30]], true);
            }
          })

        Button('重置动画')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.resetSegments(false);
            }
          })
      }.margin({ top: 5 })

      Row() {
        Button(this.animationDuration)
          .onClick(() => {
            if (this.animateItem != null) {
              console.info("getDuration:" + this.animateItem.getDuration() + 's');
              this.animationDuration = '获取动画时长' + this.animateItem.getDuration() + 's'
            }
          })

        Button(this.pauseState)
          .onClick(() => {
            if (this.animateItem != null) {
              console.info("isPaused:" + this.animateItem.isPaused);
              this.pauseState = '是否暂停：' + this.animateItem.isPaused
            }
          })
      }.margin({ top: 5 })

      Row() {
        Button('隐藏')
          .onClick(() => {
            // Canvas组件与Lottie动画隔离，目前只能通过控制Canvas组件的显示与隐藏实现对应功能。
            this.isCanvasVisible = Visibility.Hidden;
            if (this.animateItem != null) {
              this.animateItem.pause(this.animateName);
            }
          })

        Button('显示')
          .onClick(() => {
            // Canvas组件与Lottie动画隔离，目前只能通过控制Canvas组件的显示与隐藏实现对应功能。
            if (this.animateItem != null) {
              this.animateItem.play(this.animateName);
            }
            this.isCanvasVisible = Visibility.Visible;
          })
      }.margin({ top: 5 })

      Row() {
        Button('添加监听')
          .onClick(() => {
            // 添加事件监听，添加和移除的回调方法需要为同一个，否则不能正确移除
            if (this.animateItem != null) {
              this.animateItem.addEventListener('loopComplete', this.callbackItem);
            }
          })

        Button('移除监听')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.removeEventListener('loopComplete', this.callbackItem);
            }
          })
      }.margin({ top: 5 })

      Row() {
        Button('播放速度6')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.setSpeed(6);
            }
          })

        Button('播放速度1')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.setSpeed(1);
            }
          })

        Button('播放速度-1')
          .onClick(() => {
            if (this.animateItem != null) {
              this.animateItem.setSpeed(-1);
            }
          })
      }.margin({ top: 5 })

      Row() {
        Button('正向播放')
          .onClick(() => {
            lottie.setDirection(1);
          })

        Button('反向播放')
          .onClick(() => {
            lottie.setDirection(-1);
          })
      }.margin({ top: 5 })
    }
    .width('100%')
    .height('100%')
  }
}